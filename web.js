/*
 * Functions
 */

function logErrors(err, req, res, next) {
    console.error(err.stack);
    next(err);
}

function clientErrorHandler(err, req, res, next) {
    res.send(400, {
        error: "Could not decode request: JSON parsing failed"
    });
}

function validateData(data) {
    if (!data || !data.payload || !data.payload.length) {
        console.error('Empty payload.');
        return {
            success: false,
            error: 'Empty payload.'
        };
    }

    return {
        success: true
    };
}

function processData(data) {
    var result = {
        response: []
    };
    var i;
    var item;

    for (i = 0; i < data.payload.length; i++) {
        item = data.payload[i];

        if (item.drm === true && item.episodeCount && item.episodeCount > 0) {
            result.response.push({
                image: item.image ? item.image.showImage : '',
                slug: item.slug,
                title: item.title

            });
        }
    }

    return result;
}

/*
 * APIs
 */
var express = require("express");
var logfmt = require("logfmt");
var bodyParser = require("body-parser");
var app = express();

app.use(logfmt.requestLogger());
app.use(bodyParser());
app.use(logErrors);
app.use(clientErrorHandler);

app.post('/', function(req, res) {

    var data = req.body;
    var validateResult = validateData(data);
    if (!validateResult.success) {
        res.send(400, {
            error: validateResult.error
        });
        return;
    }

    var result = processData(data);

    res.send(result);
});


var port = Number(process.env.PORT || 5000);
app.listen(port, function() {
    console.log("Listening on " + port);
});